
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/homepageHW.vue') },
      { path: 'grade', component: () => import('pages/grade.vue') },
      { path: 'list', component: () => import('pages/addList.vue') },
      { path: 'sort', component: () => import('pages/sortArr.vue') }

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
